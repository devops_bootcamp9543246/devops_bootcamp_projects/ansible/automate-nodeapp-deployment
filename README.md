# 15 - Configuration Management with Ansible - Automate Node App Deployment

**Demo Project:**
Automate Node.js application Deployment

**Technologies used:**
Ansible, Node.js, DigitalOcean, Linux

**Project Description:**
- Create Server on DigitalOcean
- Write Ansible Playbook that installs necessary technologies, creates Linux user for an application and deploys a NodeJS application with that user



15 - Configuration Management with Ansible
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





